package cql.ecci.ucr.ac.cr.androidstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.Date;

public class Persona implements Parcelable {
    public static final String GENERO_MASCULINO = "M";
    public static final String GENERO_FEMENINO  = "F";
    public static final String GENERO_OTRO  = "O";

    public static final String TIPO_ADMINISTRATIVO  = "A";
    public static final String TIPO_PROFESOR = "P";
    public static final String TIPO_ESTUDIANTE = "E";
    public static final String TIPO_EXTERNO = "X";
    public static final String TIPO_SIN_DEFINIR = "S";

    private String identificacion;
    private String correo;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String telefono;
    private String celular;
    private Date fechaNacimiento;
    private String tipo; //Profesor / Administrativo / Estudiante / Externo private String genero; //Masculino / Femenino / Otro
    private String genero;

    public Persona(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String telefono, String celular, Date fechaNacimiento, String tipo, String genero) {
        this.identificacion = identificacion;
        this.correo = correo;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.telefono = telefono;
        this.celular = celular;
        this.fechaNacimiento = fechaNacimiento;
        this.tipo = tipo; this.genero = genero;
    }
    protected Persona(Parcel in) {
        setIdentificacion(in.readString());
        setCorreo(in.readString());
        setNombre(in.readString());
        setPrimerApellido(in.readString());
        setSegundoApellido(in.readString());
        setTelefono(in.readString());
        setCelular(in.readString());
        setTipo(in.readString());
    }

    public Persona(){}

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getIdentificacion());
        dest.writeString(getCorreo());
        dest.writeString(getNombre());
        dest.writeString(getPrimerApellido());
        dest.writeString(getSegundoApellido());
        dest.writeString(getTelefono());
        dest.writeString(getCelular());
        dest.writeString(getTipo());
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setGenero(String genero){
        this.genero = genero;
    }

    public String getGenero(){
        return genero;
    }

    public long insertar(Context context){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry._ID, getIdentificacion());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CORREO, getCorreo());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE, getNombre());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PRIMER_APELLIDO, getPrimerApellido());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_SEGUNDO_APELLIDO, getSegundoApellido());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_TELEFONO, getTelefono());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CELULAR, getCelular());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_FECHA_NACIMIENTO, UtilDates.DateToStringShort(getFechaNacimiento()));
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_TIPO, getTipo());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_GENERO, getGenero());
        // Insertar la nueva fila
        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, null, values);
    }

    public void leer(Context context, String identificacion){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        String[] projection = {
                DataBaseContract.DataBaseEntry._ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_CORREO,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PRIMER_APELLIDO,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_SEGUNDO_APELLIDO,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_TELEFONO,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_CELULAR,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_FECHA_NACIMIENTO,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_TIPO,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_GENERO
        };

        String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
        String[] selectionArgs = {identificacion};

        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if(cursor.moveToFirst() && cursor.getCount() > 0) {
            setIdentificacion(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry._ID)));
            setCorreo(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_CORREO)));
            setNombre(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE)));
            setPrimerApellido(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_PRIMER_APELLIDO)));
            setSegundoApellido(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_SEGUNDO_APELLIDO)));
            setTelefono(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_TELEFONO)));
            setCelular(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_CELULAR)));
            setFechaNacimiento(UtilDates.StringToDate(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_FECHA_NACIMIENTO))));
            setTipo(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_TIPO)));
            setGenero(cursor.getString(cursor.getColumnIndexOrThrow( DataBaseContract.DataBaseEntry.COLUMN_NAME_GENERO)));
        }else{
            setTipo(TIPO_SIN_DEFINIR);
        }
    }

    public void eliminar(Context context, String identificacion){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        String[] selectionArgs = {identificacion};

        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, selection, selectionArgs);
    }

    public int actualizar(Context context){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_CORREO, getCorreo());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE, getNombre());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_PRIMER_APELLIDO, getPrimerApellido());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_SEGUNDO_APELLIDO, getSegundoApellido());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_TELEFONO, getTelefono());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_CELULAR, getCelular());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_FECHA_NACIMIENTO, UtilDates.DateToStringShort(getFechaNacimiento()));
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_TIPO, getTipo());
        values.put( DataBaseContract.DataBaseEntry.COLUMN_NAME_GENERO, getGenero());
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        String[] selectionArgs = {getIdentificacion()};
        return db.update(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, values, selection, selectionArgs);
    }

    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
