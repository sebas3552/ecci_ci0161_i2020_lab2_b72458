package cql.ecci.ucr.ac.cr.androidstorage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "AndroidStorage.db";

    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataBaseContract.SQL_CREATE_PERSONA);
        db.execSQL(DataBaseContract.SQL_CREATE_ESTUDIANTE);
        db.execSQL(DataBaseContract.SQL_CREATE_FUNCIONARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DataBaseContract.SQL_DELETE_PERSONA);
        db.execSQL(DataBaseContract.SQL_DELETE_ESTUDIANTE);
        db.execSQL(DataBaseContract.SQL_DELETE_FUNCIONARIO);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
}
