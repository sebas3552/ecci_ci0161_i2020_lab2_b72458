package cql.ecci.ucr.ac.cr.androidstorage;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class UtilFiles {

    public static void guardarArchivoInterno(Context contexto, String nombre, String contenido){
        try{
            FileOutputStream fos = contexto.openFileOutput(nombre, Context.MODE_PRIVATE);
            fos.write(contenido.getBytes());
            fos.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static String leerArchivoInterno(Context contexto, String nombre){
        String readString = "";
        StringBuffer datax = new StringBuffer("");
        try{
            FileInputStream fis = contexto.openFileInput(nombre);
            int size;

            while((size = fis.read()) != -1){
                readString += Character.toString((char) size);
            }
            fis.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        return readString;
    }
}
