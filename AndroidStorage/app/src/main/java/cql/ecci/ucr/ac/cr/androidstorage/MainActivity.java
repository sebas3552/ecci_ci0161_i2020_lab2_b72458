package cql.ecci.ucr.ac.cr.androidstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonGrabarArchivo = (Button) findViewById(R.id.buttonGrabarArchivo);
        Button buttonLeerArchivo = (Button) findViewById(R.id.buttonLeerArchivo);
        Button buttonDataBaseInsert = (Button) findViewById(R.id.buttonDataBaseInsert);
        Button buttonDataBaseSelect = (Button) findViewById(R.id.buttonDataBaseSelect);
        Button buttonDataBaseUpdate = (Button) findViewById(R.id.buttonDataBaseUpdate);
        Button buttonDataBaseDelete = (Button) findViewById(R.id.buttonDataBaseDelete);

        buttonGrabarArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                grabarArchivo();
            }
        });

        buttonLeerArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerArchivo();
            }
        });

        buttonDataBaseInsert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                insertarEstudiante();
            }
        });

        buttonDataBaseUpdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                actualizarEstudiante();
            }
        });

        buttonDataBaseSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerEstudiante();
            }
        });

        buttonDataBaseDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                eliminarEstudiante();
            }
        });
    }

    private void grabarArchivo(){
        Persona persona = new Persona("1-1000-1000", "estudiante01@ucr.ac.cr", "Juan", "Perez", "Soto*", "2511-0000", "8890-0000", UtilDates.StringToDate("01 01 1995"), Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO);
        UtilFiles.guardarArchivoInterno(getApplicationContext(), "PersonaAndroidStorage.json", persona.toJson());
        Toast.makeText(getApplicationContext(), "Archivo creado: " + "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
    }

    private void leerArchivo(){
        String datosArchivo = datosArchivo = UtilFiles.leerArchivoInterno(getApplicationContext(), "PersonaAndroidStorage.json");
        if(datosArchivo.isEmpty()){
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), "Archivo: " + "PersonaAndroidStorage.json" + "Contenido: " + datosArchivo, Toast.LENGTH_LONG).show();
        }
    }

    private void insertarEstudiante(){
        Estudiante estudiante = new Estudiante("1-1000-1000", "estudiante01@ucr.ac.cr", "Juan", "Perez", "Soto", "2511-0000", "8890-0000", UtilDates.StringToDate("01 01 1995"), Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO, "A99148", 1, 8.0);

        long newRowId = estudiante.insertar(getApplicationContext());

        Toast.makeText(getApplicationContext(), "Insertar Estudiante: " + newRowId + " Id: " + estudiante.getIdentificacion() + " Carnet: " + estudiante.getCarnet() + " Nombre: " + estudiante.getNombre() + " " + estudiante.getPrimerApellido() + " " + estudiante.getSegundoApellido() + " Correo: " + estudiante.getCorreo() + " Tipo: " + estudiante.getTipo() + " Promedio: " + estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
    }

    private void leerEstudiante(){
        Estudiante estudiante = new Estudiante();

        estudiante.leer(getApplicationContext(), "1-1000-1000");

        if(estudiante.getTipo().equals(Persona.TIPO_ESTUDIANTE)){
            Toast.makeText(getApplicationContext(), "Leer Estudiante: " + estudiante.getIdentificacion() + " Carnet: " + estudiante.getCarnet() + " Nombre: " + estudiante.getNombre() + " " + estudiante.getPrimerApellido() + " " + estudiante.getSegundoApellido() + " Correo: " + estudiante.getCorreo() + " Tipo: " + estudiante.getTipo() + " Promedio: " + estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "1-1000-1000", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarEstudiante(){
        Estudiante estudiante = new Estudiante();

        estudiante.eliminar(getApplicationContext(), "1-1000-1000");

        Toast.makeText(getApplicationContext(), "Eliminar Estudiante.", Toast.LENGTH_LONG).show();
    }

    private void actualizarEstudiante(){
        Estudiante estudiante = new Estudiante("1-1000-1000", "estudiante01@ucr.ac.cr*", "Juan*", "Perez*", "Soto*", "2511-0000*", "8890-0000*", UtilDates.StringToDate("01 01 1995"), Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO, "A99148*", 1, 8.0);

        int contador = estudiante.actualizar(getApplicationContext());

        if(contador > 0){
            Toast.makeText(getApplicationContext(), "Actualizar Estudiante: " + contador + " Id: " + estudiante.getIdentificacion() + " Carnet: " + estudiante.getCarnet() + " Nombre: " + estudiante.getNombre() + " " + estudiante.getPrimerApellido() + " " + estudiante.getSegundoApellido() + " Correo: " + estudiante.getCorreo() + " Tipo: " + estudiante.getTipo() + " Promedio: " + estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), "No hay datos para: " + estudiante.getIdentificacion(), Toast.LENGTH_LONG).show();
        }
    }
}
